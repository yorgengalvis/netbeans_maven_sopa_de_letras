/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;
import java.io.FileInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.*;
/**
 *
 * @author yorge_000
 */
public class Matriz_Excel_Ejemplo {
     public static String leerExcel(String ruta) throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(ruta));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        //System.out.println("Filas:"+canFilas);
        String co="";
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol=filas.getLastCellNum();
            String valor = null;
        for(int j=0;j<cantCol;j++)    
        {
            //Obtiene la celda y su valor respectivo
            //double r=filas.getCell(i).getNumericCellValue();
            valor=filas.getCell(j).getStringCellValue();
            //me guarda el valor
            co+=valor;
            //System.out.print(valor);
        }
        //me lo separa con comas al finalizar la fila
        co+=",";
       }
        return co;
    }
}
